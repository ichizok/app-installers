#!/bin/bash -e

_MSG() {
    echo -e "\e[32;1m$@\e[0m"
}

_LOG() {
    echo -e "\e[37;1m$@\e[0m"
}

_ERR() {
    echo -e "\e[31;1m$@\e[0m"
}

_LET() {
    _LOG "$@"
    eval "$@"
}

version=$1
if ! [[ ${version} ]]; then
    echo >&2 "Usage: ${0##*/} version"
    exit 1
fi

install_dir=/usr/local/stow/fd
archive_name=fd-v${version}-x86_64-unknown-linux-musl.tar.gz
resource_url=https://github.com/sharkdp/fd/releases/download/v${version}/${archive_name}

_MSG "===> Prepare working directory..."
workdir=$(mktemp -d)
_LET cd "${workdir}"

_MSG "===> Download fd-${version}..."
_LET wget "${resource_url}"
_LET tar xvf "${archive_name}"

_MSG "===> Install fd-${version}..."
_LET cd "${archive_name%.tar.gz}"
fd_bin=fd
fd_man=fd.1
_LET install -d "${install_dir}/bin"
_LET install -d "${install_dir}/share/man/man1"
_LET install -m 755 "${fd_bin}" "${install_dir}/bin"
_LET install -m 644 "${fd_man}" "${install_dir}/share/man/man1"

_MSG "===> Cleanup..."
_LET rm -r "${workdir}"
