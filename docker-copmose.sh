#!/bin/bash -e

_MSG() {
    echo -e "\e[32;1m$@\e[0m"
}

_LOG() {
    echo -e "\e[37;1m$@\e[0m"
}

_ERR() {
    echo -e "\e[31;1m$@\e[0m"
}

_LET() {
    _LOG "$@"
    eval "$@"
}

version=$1
if ! [[ ${version} ]]; then
    echo >&2 "Usage: ${0##*/} version [install_dir]"
    exit 1
fi

resource_url=https://github.com/docker/compose/releases/download/${version}/docker-compose-$(uname -s)-$(uname -m)
install_dir=${2:-/usr/local/stow/docker-compose/bin}
workdir=$(mktemp -d)
outfile=${workdir}/docker-compose

_MSG "===> Download docker-compose-${version}..."
_LET curl -L -o "${outfile}" "${resource_url}"

_MSG "===> Install docker-compose-${version}..."
_LET install -d "${install_dir}"
_LET install -m 755 "${outfile}" "${install_dir}"

_MSG "===> Cleanup..."
_LET rm -r "${workdir}"
